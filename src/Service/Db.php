<?php

namespace GranitSDK\Service;

use GranitSDK\Config;

class Db extends \Phalcon\Db\Adapter\Pdo\Mysql
{
	public function __construct()
	{
		\Phalcon\Mvc\Model::setup([
			'notNullValidations' => false
		]);

		parent::__construct([
			'host'     => Config::get()->getMySQL()->getHost(),
			'username' => Config::get()->getMySQL()->getUser(),
			'password' => Config::get()->getMySQL()->getPass(),
			'dbname'   => Config::get()->getMySQL()->getDb(),
			'options' => [
				\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
			],
			'charset'  => 'utf8'
		]);
	}
}