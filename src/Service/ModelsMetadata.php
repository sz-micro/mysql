<?php

namespace GranitSDK\Service;

use Phalcon\Mvc\Model\MetaData\Memory;

class ModelsMetadata extends \Phalcon\Mvc\Model\Manager
{
	public static function get()
	{
		return new Memory();
	}
}