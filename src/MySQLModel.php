<?php

namespace GranitSDK;

abstract class MySQLModel extends \Phalcon\Mvc\Model
{
	const RECORD_STATUS_ACTIVE = 1;
	const RECORD_STATUS_DELETED = -1;

	protected $id;
	protected $record_status = self::RECORD_STATUS_ACTIVE;

	public static $executionModelCacheArray = array();
	public static $useExecutionModelCache   = true;
	public static $useMemoryModelCache      = true;
	public static $useStatusFilter          = true;

	public function getId(){return $this->id;}

	abstract protected function SpotValidation();
	abstract protected function HackValidation();

	public function getStatus()
	{
		return $this->record_status;
	}

	public function setStatus($status): self
	{
		$this->record_status = $status;
		return $this;
	}

	public function setStatusToActive(): self
	{
		$this->record_status = static::RECORD_STATUS_ACTIVE;
		return $this;
	}

	public function bind(array $dataArray = array())
	{
		foreach ($dataArray as $attribute => $value) {
			if (property_exists($this, $attribute)) {
				$this->{$attribute} = $value;
			}
		}
	}

	public function beforeValidation()
	{
		$hackReturn = $this->HackValidation();
		if ($hackReturn === false) {
			return false;
		}
	}

	public function delete():bool
	{
		$this->beforeDelete();
		$this->record_status = static::RECORD_STATUS_DELETED;
		return $this->save();
	}

	public function save($data = null, $whiteList = null):bool
	{
		try {
			$result = parent::save($data, $whiteList);
			if (!$result) {
				throw new \Exception($this->getMessages()[0]->getMessage());
			}
			return $result;
		}
		catch (\PDOException $e) {
			throw new \Exception($e->getMessage());
		}

		return false;
	}

	/**
	 * @return $this|false
	 */
	public static function getById($id)
	{
		return static::findFirst([
			'id = :id:',
			'bind' => [
				'id' => $id
			]
		]);
	}

	/**
	 * @return static $this
	 */
	public static function getByIdOrNew($id)
	{
		$model = static::getById($id);
		if (!$model) {
			$model = new static();
		}
		return $model;
	}

	private static function getModelUniqueKeyByParameter($parameters = array())
	{
		return md5(serialize(array(static::class, $parameters)));
	}

	private static function isExistsInExecutionCacheByParameters($parameters = array())
	{
		if (!static::$useExecutionModelCache) {
			return false;
		}

		return isset(static::$executionModelCacheArray[static::getModelUniqueKeyByParameter($parameters)]);
	}

	private static function getExecutionCacheByParameters($parameters = array())
	{
		return static::$executionModelCacheArray[static::getModelUniqueKeyByParameter($parameters)];
	}

	private static function setExecutionCacheByParameters($parameters = array(), $result)
	{
		static::$executionModelCacheArray[static::getModelUniqueKeyByParameter($parameters)] = $result;
	}

	private static function isExistsInMemCacheByParameters($parameters = array())
	{
		if (!static::$useMemoryModelCache) {
			return false;
		}

		return MemCached::get()->has(static::getModelUniqueKeyByParameter($parameters));
	}

	private static function getCacheKeyListMemcacheKey()
	{
		return static::class . '_keys';
	}

	private static function appendMemoryCacheKey($newKey)
	{
		$alreadySavedKeys   = MemCached::get()->get(static::getCacheKeyListMemcacheKey());

		if (empty($alreadySavedKeys)) {
			$alreadySavedKeys = [];
		}

		if (!in_array($newKey, $alreadySavedKeys)) {
			$alreadySavedKeys[] = $newKey;
		}

		MemCached::get()->set(static::getCacheKeyListMemcacheKey(), $alreadySavedKeys);
	}


	private static function getFromMemCacheByParameters($parameters = array())
	{
		return MemCached::get()->get(static::getModelUniqueKeyByParameter($parameters));
	}

	private static function setMemCacheByParameters($parameters = array(), $result)
	{
		if (!static::$useMemoryModelCache) {
			return false;
		}

		MemCached::get()->set(static::getModelUniqueKeyByParameter($parameters), $result);

		static::appendMemoryCacheKey(static::getModelUniqueKeyByParameter($parameters));
	}

	public static function clearMemoryCache()
	{
		if (!static::$useMemoryModelCache) {
			return false;
		}
		$alreadySavedKeys   = MemCached::get()->get(static::getCacheKeyListMemcacheKey());
		if (empty($alreadySavedKeys)) {
			$alreadySavedKeys = array();
		}
		foreach ($alreadySavedKeys as $key) {
			MemCached::get()->delete($key);
		}
	}

	public function afterSave()
	{
		$class = explode('\\', get_class($this));
		$class = end($class);
		$spicaKey = $class . ':1:' . $this->id;

		MemCached::get()->delete($spicaKey);

		static::clearMemoryCache();
	}

	public function afterDelete()
	{
		static::clearMemoryCache();
	}

	private static function addConditionToFind($condition, &$findParameters)
	{
		if(isset($findParameters['conditions'])){
			$findParameters['conditions'] .= ' ' . $condition . ' ';
			return;
		}

		if (empty($findParameters[0])) {
			$findParameters[0] = ' 1 ';
		}

		$findParameters[0] .= ' ' . $condition . ' ';
	}

	protected static function getFromCacheByParameters($parameters)
	{
		if (static::isExistsInExecutionCacheByParameters($parameters)) {
			return static::getExecutionCacheByParameters($parameters);
		}

		if (static::isExistsInMemCacheByParameters($parameters)){
			return static::getFromMemCacheByParameters($parameters);
		}
	}

	protected static function setToCacheByParameters($parameters, $result)
	{
		static::setExecutionCacheByParameters($parameters, $result);
		static::setMemCacheByParameters($parameters, $result);
	}

	protected static function notActiveCondition()
	{
		return 'AND record_status = ' . static::RECORD_STATUS_ACTIVE;
	}

	public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
	{
		if (!is_array($parameters)) {
			$parameters = array($parameters);
		}

		if (static::$useStatusFilter) {
			static::addConditionToFind(static::notActiveCondition(), $parameters);
		}
		$parameters['queryTypeForCacheKey'] = 'find';

		if ($resultFromCache = static::getFromCacheByParameters($parameters)) {
			return $resultFromCache;
		}

		$result = parent::find($parameters);

		static::setToCacheByParameters($parameters, $result);

		return $result;
	}

	public static function findFirst($parameters = null): ?\Phalcon\Mvc\ModelInterface
	{
		if (!is_array($parameters)) {
			$parameters = array($parameters);
		}

		if (static::$useStatusFilter) {
			static::addConditionToFind(static::notActiveCondition(), $parameters);
		}

		$parameters['queryTypeForCacheKey'] = 'findFirst';

		if ($resultFromCache = static::getFromCacheByParameters($parameters)) {
			return $resultFromCache;
		}

		$result = parent::findFirst($parameters);

		//static::setToCacheByParameters($parameters, $result);

		return $result;
	}

	public static function count($parameters = null):int
	{
		if (!is_array($parameters)) {
			$parameters = array($parameters);
		}

		if (static::$useStatusFilter) {
			static::addConditionToFind(static::notActiveCondition(), $parameters);
		}

		$parameters['queryTypeForCacheKey'] = 'count';

		if ($resultFromCache = static::getFromCacheByParameters($parameters)) {
			return $resultFromCache;
		}

		$result = parent::count($parameters);

		static::setToCacheByParameters($parameters, $result);

		return $result;
	}

	/**
	 * @return self[]
	 */
	public static function findByIds($ids = [])
	{
		if (!$ids) {
			return [];
		}

		return static::find([
			'conditions' => 'id in (' . implode(',', $ids) . ')',
		]);
	}
}